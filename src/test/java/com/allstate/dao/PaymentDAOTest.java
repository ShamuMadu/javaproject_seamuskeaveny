package com.allstate.dao;

import com.allstate.dto.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentDAOTest {

    @Autowired
    IPaymentDAO dao;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void cleanUp() {
        if (!(mongoTemplate == null)) {
            for (String collectionName : mongoTemplate.getCollectionNames()) {
                if (collectionName.equals("payment")) {
                    mongoTemplate.dropCollection(collectionName);
                }
            }
        }
    }

    @Test
    public void rowCountTest() {
        Payment paymentOne = new Payment(1, new Date(), "cash", 99.99, 2);
        Payment paymentTwo = new Payment(2, new Date(), "cheque", 40.00, 2);
        Payment paymentThree = new Payment(3, new Date(), "credit card", 46.34, 3);

        // calling dao rather than mongoTemplate (integration testing rather than unit testing)
        dao.save(paymentOne);
        dao.save(paymentTwo);
        dao.save(paymentThree);

        Long rowCount = dao.rowCount();
        Assert.assertEquals("RowCount did not return a count of 3 payments.", 3L, rowCount.longValue());
    }

    @Test
    public void findByIdTest() {
        Payment payment = new Payment(1, new Date(), "cash", 99.99, 2);

        // calling dao rather than mongoTemplate (integration testing rather than unit testing)
        dao.save(payment);

        Payment savedPayment = dao.findById(payment.getId());
        Assert.assertNotNull("Payment with Id of 1 was not retrieved successfully.", savedPayment);
    }

    @Test
    public void findAllTest() {
        Payment paymentOne = new Payment(1, new Date(), "cash", 99.99, 2);
        Payment paymentTwo = new Payment(2, new Date(), "cheque", 40.00, 2);
        Payment paymentThree = new Payment(3, new Date(), "cash", 66.99, 3);

        // calling dao rather than mongoTemplate (integration testing rather than unit testing)
        dao.save(paymentOne);
        dao.save(paymentTwo);
        dao.save(paymentThree);

        List<Payment> payments = dao.findAll();
        Assert.assertEquals("FindAll did not return 3 payments.", 3, payments.size());
    }

    @Test
    public void findByTypeTest() {
        Payment paymentOne = new Payment(1, new Date(), "cash", 99.99, 2);
        Payment paymentTwo = new Payment(2, new Date(), "cheque", 40.00, 2);
        Payment paymentThree = new Payment(3, new Date(), "cash", 66.99, 3);

        // calling dao rather than mongoTemplate (integration testing rather than unit testing)
        dao.save(paymentOne);
        dao.save(paymentTwo);
        dao.save(paymentThree);

        List<Payment> payments = dao.findByType("cash");
        Assert.assertEquals("FindByType did not return 2 \"cash\" payments.", 2, payments.size());
    }

    @Test
    public void saveTest() {
        Payment payment = new Payment(1, new Date(), "cash", 99.99, 2);

        dao.save(payment);

        // calling dao rather than mongoTemplate (integration testing rather than unit testing)
        Long count = dao.rowCount();
        Assert.assertEquals("Payment was not saved successfully.", 1L, count.longValue());
    }
}
