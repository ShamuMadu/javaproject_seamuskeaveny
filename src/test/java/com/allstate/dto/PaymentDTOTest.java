package com.allstate.dto;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class PaymentDTOTest {

    @Test
    public void paymentToStringTest() {
        Payment payment = new Payment(1, new Date(0), "cash", 99.99, 2);
        Assert.assertEquals(
            "Payment .toString() does not match.",
            "Payment 1 Thu Jan 01 00:00:00 UTC 1970 cash 99.99 2",
            payment.toString());
    }
}
