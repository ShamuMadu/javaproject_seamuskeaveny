package com.allstate.services;

import com.allstate.dto.Payment;
import com.allstate.exceptions.InvalidPaymentIdException;
import com.allstate.exceptions.InvalidPaymentTypeException;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentServicesTest {

    @Autowired
    IPaymentService service;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void cleanUp() {
        if (!(mongoTemplate == null)) {
            for (String collectionName : mongoTemplate.getCollectionNames()) {
                if (collectionName.equals("payment")) {
                    mongoTemplate.dropCollection(collectionName);
                }
            }
        }
    }

    @Test
    public void rowCountTest() {
        Payment payment = new Payment(1, new Date(), "cash", 99.99, 2);

        // calling service rather than mongoTemplate (integration testing rather than unit testing)
        service.save(payment);

        Long count = service.rowCount();
        Assert.assertEquals("RowCount was not 1.", 1L, count.longValue());
    }

    @Test
    public void findAllTest() {
        Payment paymentOne = new Payment(1, new Date(), "cash", 99.99, 2);
        Payment paymentTwo = new Payment(2, new Date(), "cheque", 40.00, 2);
        Payment paymentThree = new Payment(3, new Date(), "cash", 66.99, 3);

        // calling service rather than mongoTemplate (integration testing rather than unit testing)
        service.save(paymentOne);
        service.save(paymentTwo);
        service.save(paymentThree);

        List<Payment> payments = service.findAll();
        Assert.assertEquals("FindAll did not return 3 payments.", 3, payments.size());
    }

    @Test
    public void findByIdTest() {
        Payment payment = new Payment(1, new Date(), "cash", 99.99, 2);

        // calling service rather than mongoTemplate (integration testing rather than unit testing)
        service.save(payment);

        Payment savedPayment = service.findById(payment.getId());
        Assert.assertNotNull("Payment with Id of 1 was not retrieved successfully.", savedPayment);
    }

    @Test
    public void findByIdInvalidIdOneTest() {
        Payment payment = new Payment(1, new Date(), "cash", 99.99, 2);

        // calling service rather than mongoTemplate (integration testing rather than unit testing)
        service.save(payment);

        Assert.assertThrows("FindById should throw InvalidPaymentIdException when Id is not greater than zero.", InvalidPaymentIdException.class, () -> service.findById(-1));
    }

    @Test
    public void findByIdInvalidIdTwoTest() {
        Payment payment = new Payment(1, new Date(), "cash", 99.99, 2);

        // calling service rather than mongoTemplate (integration testing rather than unit testing)
        service.save(payment);

        Assert.assertThrows("FindById should throw InvalidPaymentIdException when Id is not greater than zero.", InvalidPaymentIdException.class, () -> service.findById(0));
    }

    @Test
    public void findByTypeTest() {
        Payment paymentOne = new Payment(1, new Date(), "cash", 99.99, 2);
        Payment paymentTwo = new Payment(2, new Date(), "cheque", 40.00, 2);
        Payment paymentThree = new Payment(3, new Date(), "cash", 66.99, 3);

        // calling service rather than mongoTemplate (integration testing rather than unit testing)
        service.save(paymentOne);
        service.save(paymentTwo);
        service.save(paymentThree);

        List<Payment> payments = service.findByType("cash");
        Assert.assertEquals("FindByType did not return 2 payments for \"cash\" type.", 2, payments.size());
    }

    @Test
    public void findByTypeInvalidTypeTest() {
        Payment paymentOne = new Payment(1, new Date(), "cash", 99.99, 2);
        Payment paymentTwo = new Payment(2, new Date(), "cheque", 40.00, 2);
        Payment paymentThree = new Payment(3, new Date(), "cash", 66.99, 3);

        // calling service rather than mongoTemplate (integration testing rather than unit testing)
        service.save(paymentOne);
        service.save(paymentTwo);
        service.save(paymentThree);

        Assert.assertThrows("FindByType should throw InvalidPaymentTypeException when Type is null.", InvalidPaymentTypeException.class, () -> service.findByType(null));
    }

    @Test
    public void saveTest() {
        Payment payment = new Payment(1, new Date(), "cash", 99.99, 2);
        service.save(payment);

        // calling service rather than mongoTemplate (integration testing rather than unit testing)
        Long count = service.rowCount();
        Assert.assertEquals("Payment was not saved successfully.", 1L, count.longValue());
    }
}
