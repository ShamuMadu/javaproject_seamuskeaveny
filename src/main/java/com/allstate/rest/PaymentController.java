package com.allstate.rest;

import com.allstate.dto.Payment;
import com.allstate.services.IPaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/payment")
public class PaymentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    IPaymentService service;

    @RequestMapping(value="/status", method= RequestMethod.GET)
    public String status() {
        LOGGER.info("Payment Controller Rest Api is running.");
        return "Payment Controller Rest Api is running";
    }

    @RequestMapping(value="/count", method= RequestMethod.GET)
    public Long rowcount() {
        LOGGER.debug("Calling count method.");
        return service.rowCount();
    }

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    public Payment findById(@PathVariable("id") int id) {
        LOGGER.debug("Calling findById method for id " + id + ".");
        return service.findById(id);
    }

    @RequestMapping(value="/find/{type}", method= RequestMethod.GET)
    public List<Payment> findByType(@PathVariable("type") String type) {
        LOGGER.debug("Calling findByType method for type \"" + type + "\".");
        return service.findByType(type);
    }

    @RequestMapping(value="/all", method= RequestMethod.GET)
    public List<Payment> findAll() {
        LOGGER.debug("Calling findAll method.");
        return service.findAll();
    }

    @RequestMapping(value="/save", method= RequestMethod.POST)
    public int save(@RequestBody Payment payment) {
        LOGGER.debug("Calling save method for payment \"" + payment.toString() + "\".");
        return service.save(payment);
    }
}
