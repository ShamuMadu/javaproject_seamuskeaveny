package com.allstate.services;

import com.allstate.dto.Payment;

import java.util.List;

public interface IPaymentService {
    Long rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    List<Payment> findAll();
    int save(Payment payment);
}
