package com.allstate.services;

import com.allstate.dao.IPaymentDAO;
import com.allstate.dto.Payment;
import com.allstate.exceptions.InvalidPaymentIdException;
import com.allstate.exceptions.InvalidPaymentTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentService implements IPaymentService {

    @Autowired
    IPaymentDAO dao;

    @Override
    public Long rowCount() {
        return dao.rowCount();
    }

    @Override
    public Payment findById(int id) {
        if (!(id > 0)) {
            throw new InvalidPaymentIdException("Payment Id must be greater than zero.");
        }
        return dao.findById(id);
    }

    @Override
    public List<Payment> findByType(String type) {
        if (type == null)
        {
            throw new InvalidPaymentTypeException("Payment Type must not be null.");
        }
        return dao.findByType(type);
    }

    @Override
    public List<Payment> findAll() {
        return dao.findAll();
    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }
}
