package com.allstate.dao;

import com.allstate.dto.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDAO implements IPaymentDAO {

    @Autowired
    private MongoTemplate template;

    @Override
    public Long rowCount() {
        Query query = new Query();
        Long result = template.count(query, Payment.class);
        return result;
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = template.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> payments = template.find(query, Payment.class);
        return payments;
    }

    @Override
    public List<Payment> findAll() {
        List<Payment> payments = template.findAll(Payment.class);
        return payments;
    }

    @Override
    public int save(Payment payment) {
        template.save(payment);
        return payment.getId();
    }
}
