package com.allstate.exceptions;

public class InvalidPaymentTypeException extends RuntimeException {
    private static final long serialVersionUID = 2L;

    public InvalidPaymentTypeException(String message) {
        super(message);
    }
}
