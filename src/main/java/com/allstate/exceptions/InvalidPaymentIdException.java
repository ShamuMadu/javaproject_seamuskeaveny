package com.allstate.exceptions;

public class InvalidPaymentIdException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InvalidPaymentIdException(String message) {
        super(message);
    }
}
